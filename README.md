**This project is a monitoring software to keep track of temperature and air quality of a room with a pi and arduinos**

We work with a raspberry pi to keep track of sensor data from a arduino

We use a few different sensors to get interesting live data

We use a database to keep the metrics stored with a timestamp

Python is a very good goto since the program should be executed as a cronjob and we don't want many unnecessary dependencies

Implement a pipeline with automated functionality

The gitlab pipeline is used to keep our program secure and focus on our tests and code quality

Working on an alerting system for threshold values. Instead of an email server we wanted to use something more modern and decided to use discord

For last we plan on using grafana as a GUI to have a visual view on the metrics

UML for pipeline and the software itself


We want to challenge ourself with new technology and a live hardware medium. Having different technologies and connect them efficiently is what we want to learn in this Lernfeld. 

