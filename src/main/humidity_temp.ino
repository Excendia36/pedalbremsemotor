/* Arduino example code for DHT11, DHT22/AM2302 and DHT21/AM2301 temperature and humidity sensors. More info: www.www.makerguides.com */

// Include the libraries:
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include "MQ135.h"


// Set DHT Digital pin:
#define DHTPIN 3

// Set MQ-135 Analog pin:
#define MQPIN A5

// Set DHT type, uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11 

// Initialize DHT sensor for normal 16mhz Arduino:
DHT dht = DHT(DHTPIN, DHTTYPE);

// Initialize MQ135 gas-sensor:
MQ135 gasSensor = MQ135(MQPIN);

void setup() {
  // Begin serial communication at a baud rate of 9600:
  Serial.begin(9600);

  // Setup sensor:
  dht.begin();
}

void loop() {
    // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)

  // Read the humidity in %:
  float h = dht.readHumidity();
  // Read the temperature as Celsius:
  float t = dht.readTemperature();
  //Read the C02 level in ppm
  float rzero = gasSensor.getCorrectedRZero(h,t);
  
  // Check if any reads failed and exit early (to try again):
  if (isnan(h) || isnan(t)|| isnan(rzero)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" % ");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print(" \xC2\xB0");
  Serial.print("C ");
  Serial.print("rzero: ");
  Serial.print(rzero);
  Serial.print("\n");
  
  // Wait a few seconds between measurements:
  delay(2000); 
}
