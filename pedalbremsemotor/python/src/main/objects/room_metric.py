"""This module handles the Room metrics."""


class RoomMetric:
    """Room metric class"""
    def __init__(self, temperature, humidity, co2):
        """init method"""
        self.temperature = temperature
        self.humidity = humidity
        self.co2 = co2

    def __enter__(self):
        """enter method"""
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        """exit method"""
        return True

    def get_temperature(self):
        """returns temperature"""
        return self.temperature

    def get_humidity(self):
        """returns humidity"""
        return self.humidity

    def get_co2(self):
        """returns co2"""
        return self.co2
