"""This module handles the discord alert bot."""
from discord_webhook.webhook import DiscordWebhook  # pylint: disable=E0401

WEBHOOK_URL = """https://discord.com/api/webhooks/887604694095122453
/CjczhIDTxArXSpcC09z_M8MJKkI4k5fZFLXGZMVqV2Kxe04WNRJhQpl3rjxyWPSNXVIR """


def send_alert(alert_message):
    """This method sends the alert message via the discord bot"""
    webhook = DiscordWebhook(url=WEBHOOK_URL,
                             content=alert_message)
    webhook.execute()
