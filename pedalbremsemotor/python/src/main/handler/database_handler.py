"""This module handles the Database functionality"""
import mysql.connector


def insert_room_metrics(room_metric):
    """This method handles the databases insertions"""
    connection = mysql.connector.connect(host='localhost', database='Pedalbremsemotor',
                                         user='python_script', password="8.eD-?8ALR%t2{uy")
    cursor = connection.cursor()

    add_humidity = "INSERT INTO humidity_sensor (humidity, threshold_level) VALUES (%s, %s)"
    val = (room_metric.get_humidity(), "1")
    cursor.execute(add_humidity, val)

    add_temperature = "INSERT INTO temperatur_sensor (temperature, threshold_level) VALUES (%s, %s)"
    val = (room_metric.get_temperature(), "1")
    cursor.execute(add_temperature, val)

    add_ppm = "INSERT INTO co2_sensor (co2_level, air_quality) VALUES (%s, %s)"
    val = (room_metric.get_co2(), "1")
    cursor.execute(add_ppm, val)

    connection.commit()
    cursor.close()
    connection.close()
