"""This module handles the sensor data"""
from pedalbremsemotor.python.src.main.sensor import sensor
from pedalbremsemotor.python.src.main.objects import room_metric
from pedalbremsemotor.python.src.main.handler import database_handler
from pedalbremsemotor.python.src.main.alerting import alert


# get temperature,humidty and airquality of Room
def retrieve_current_metrics():
    """This method retrieves the sensor data"""
    temperature = float(sensor.get_temperature())
    humidity = float(sensor.get_humidity())
    co2 = float(sensor.get_co2())

    return room_metric.RoomMetric(temperature, humidity, co2)


def check_thresholds(metrics):
    """This method calls the threshold checker of alarm module"""
    alert.check_thresholds(metrics)


def insert_room_metrics_in_db(metrics):
    """This method calls the database insertion of database_handler"""
    database_handler.insert_room_metrics(metrics)


with retrieve_current_metrics() as room_metrics:
    insert_room_metrics_in_db(room_metrics)
    check_thresholds(room_metrics)
