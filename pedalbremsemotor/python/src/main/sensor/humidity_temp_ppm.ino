/* Arduino example code for DHT11, DHT22/AM2302 and DHT21/AM2301 temperature and humidity sensors. More info: www.www.makerguides.com */

// Include the libraries:
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include "MQ135.h"


// Set DHT Digital pin:
#define DHTPIN 3

// Set MQ-135 Analog pin:
#define MQPIN A5

// Set DHT type, uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11 

// Initialize DHT sensor for normal 16mhz Arduino:
DHT dht = DHT(DHTPIN, DHTTYPE);

// Initialize MQ135 gas-sensor:
MQ135 gasSensor = MQ135(MQPIN);

void setup() {
  // Begin serial communication at a baud rate of 9600:
  Serial.begin(9600);

  // Setup sensor:
  dht.begin();
}

void loop() {
    while(Serial.available() == 0) {
    }
    char in=Serial.read();
    switch (in){
      case 'h':{
        float humidity = dht.readHumidity();        // Read humidity in %
        Serial.println(humidity);
        Serial.flush();
      }
       break;
     case 't':{
        float temperature = dht.readTemperature();    // Read the temperature as Celsius
        Serial.println(temperature);
        Serial.flush();
     }
        break;
     case 'p':{
        float ppm=get_ppm();                   //Read the C02 level in ppm
        Serial.println(ppm);
        Serial.flush();
    }
        break;
  }
  delay(2000);
}

// Caclulates the ppm value under consideration of humidity and temperature
float get_ppm(){
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  float ppm = gasSensor.getCorrectedPPM(h,t);
  return ppm;
}
