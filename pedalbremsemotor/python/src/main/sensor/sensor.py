"""This module handles the sensors connected to the Arduino."""

import time
import serial
# Port of the Raspberry-Pi connected to Arduino
SERIAL_PORT = '/dev/ttyACM0'


def initialize_sensor_port():
    """This module initializes the sensor."""
    ser = serial.Serial('/dev/ttyACM0', 9600)
    ser.close()
    ser.open()
    time.sleep(2)
    return ser


def get_sensor_data(serial_port, data_label):
    """returns wanted sensor data with specific data label """
    serial_port.write(data_label)
    data = serial_port.readline()
    return data.decode('utf-8')


def get_temperature():
    """gathers and returns temperature"""
    serial_port = initialize_sensor_port()
    temperature = get_sensor_data(serial_port, b't')
    return temperature


def get_humidity():
    """gathers and returns humidity"""
    serial_port = initialize_sensor_port()
    humidity = get_sensor_data(serial_port, b'h')
    return humidity


def get_co2():
    """gathers and returns co2"""
    serial_port = initialize_sensor_port()
    co2 = get_sensor_data(serial_port, b'p')
    return co2
