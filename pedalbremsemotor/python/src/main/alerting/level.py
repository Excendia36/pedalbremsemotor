"""This module handles the level Enums"""
from enum import Enum


class Level(Enum):
    """Class for the level Enums"""
    LOW = "LOW"
    MEDIUM = "MEDIUM"
    CRITICAL = "CRITICAL"
