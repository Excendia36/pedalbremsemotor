"""This module handles the alert functionality"""
import os
import configparser
from pathlib import Path
from datetime import datetime
from pedalbremsemotor.python.src.main.alerting.level import Level as level
from pedalbremsemotor.python.src.main.handler import discord_handler as mailHandler

# Path of project root
ROOT_DIR = Path(__file__).parents[5]

CONFIG_PATH = os.path.join(ROOT_DIR, 'config', 'threshold_config.ini')
config = configparser.ConfigParser()
config.read(CONFIG_PATH)


def check_temperature(temperature):
    """ method to check if the temperature value triggers a alert"""
    temperature = float(temperature)
    minimum_threshold = float(config['Temperature']['minimum'])
    ok_threshold = float(config['Temperature']['ok'])
    maximum_threshold = float(config['Temperature']['max'])

    if temperature < minimum_threshold:
        send_alert('temperature', level.LOW, temperature)
    if ok_threshold < temperature < maximum_threshold:
        send_alert('temperature', level.MEDIUM, temperature)
    if temperature > maximum_threshold:
        send_alert('temperature', level.CRITICAL, temperature)


def check_humidity(humidity):
    """ method to check if the humidity value triggers a alert"""
    humidity = float(humidity)
    minimum_threshold = float(config['Humidity']['minimum'])
    ok_threshold = float(config['Humidity']['ok'])
    maximum_threshold = float(config['Humidity']['max'])

    if humidity < minimum_threshold:
        send_alert('humidity', level.LOW, humidity)
    if ok_threshold < humidity < maximum_threshold:
        send_alert('humidity', level.MEDIUM, humidity)
    if humidity > maximum_threshold:
        send_alert('humidity', level.CRITICAL, humidity)


def check_co2(co2):
    """ method to check if the co2 value triggers a alert"""
    co2 = float(co2)
    minimum_threshold = float(config['CO2']['minimum'])
    ok_threshold = float(config['CO2']['ok'])
    maximum_threshold = float(config['CO2']['max'])

    if co2 < minimum_threshold:
        send_alert('air-quality', level.LOW, co2)
    if ok_threshold < co2 < maximum_threshold:
        send_alert('air-quality', level.MEDIUM, co2)
    if co2 > maximum_threshold:
        send_alert('air-quality', level.CRITICAL, co2)


def check_thresholds(room_metric):
    """method the handles the threshold checks for the room values"""
    with room_metric.get_temperature() as temperature:
        check_temperature(temperature)

    with room_metric.get_humidity() as humidity:
        check_humidity(humidity)

    with room_metric.get_co2() as co2:
        check_co2(co2)


def send_alert(metricname, level_name, value):
    """method to handles the alert messages"""
    # implement alert logic with the metric which reached a threshold and the urgency (level)
    timestamp = datetime.now().strftime("%Y-%m-%d, %H:%M:%S")
    warning_message = (timestamp + ":" + "[Warning] " + metricname + " reached "
                       + str(level_name.value) + " value (" + str(value) + ").\n")
    logfile_path = os.path.join(ROOT_DIR, 'alert_log.txt')
    with open(logfile_path, "a", encoding='utf-8') as logfile:
        logfile.write(warning_message)
        logfile.close()

    mailHandler.send_alert(warning_message)
