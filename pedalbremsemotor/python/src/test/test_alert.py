"""This module handles the unit tests for the Alert."""

import unittest
from unittest.mock import patch
from pedalbremsemotor.python.src.main.alerting import alert
from pedalbremsemotor.python.src.main.alerting.level import Level as level


class TemperatureAlertTestCase(unittest.TestCase):
    """the Class for the temperature Test Cases."""
    @staticmethod
    def test_temperature_low():
        """the testcase for the low temperature."""
        with patch("pedalbremsemotor.python.src.main.alerting.alert.send_alert") \
                as patched_function:
            temperature = "20"
            alert.check_temperature(temperature)
        patched_function.assert_called_with('temperature', level.LOW, float(temperature))

    @staticmethod
    def test_temperature_medium():
        """the testcase for the medium temperature."""
        with patch("pedalbremsemotor.python.src.main.alerting.alert.send_alert") \
                as patched_function:
            temperature = "35"
            alert.check_temperature(temperature)
        patched_function.assert_called_with('temperature', level.MEDIUM, float(temperature))

    @staticmethod
    def test_temperature_critical():
        """the testcase for the critical temperature."""
        with patch("pedalbremsemotor.python.src.main.alerting.alert.send_alert") \
                as patched_function:
            temperature = "45"
            alert.check_temperature(temperature)
        patched_function.assert_called_with('temperature', level.CRITICAL, float(temperature))


class HumidityAlertTestCase(unittest.TestCase):
    """the Class for the humidity Test Cases."""
    @staticmethod
    def test_humidity_low():
        """the testcase for the low humidity."""
        with patch("pedalbremsemotor.python.src.main.alerting.alert.send_alert") \
                as patched_function:
            humidity = "39"
            alert.check_humidity(humidity)
        patched_function.assert_called_with('humidity', level.LOW, float(humidity))

    @staticmethod
    def test_humidity_medium():
        """the testcase for the medium humidity."""
        with patch("pedalbremsemotor.python.src.main.alerting.alert.send_alert") \
                as patched_function:
            humidity = "61"
            alert.check_humidity(humidity)
        patched_function.assert_called_with('humidity', level.MEDIUM, float(humidity))

    @staticmethod
    def test_humidity_critical():
        """the testcase for the critical humidity."""
        with patch("pedalbremsemotor.python.src.main.alerting.alert.send_alert") \
                as patched_function:
            humidity = "80"
            alert.check_humidity(humidity)
        patched_function.assert_called_with('humidity', level.CRITICAL, float(humidity))


class AirQualityAlertTestCase(unittest.TestCase):
    """the Class for the air quality Test Cases."""
    @staticmethod
    def test_air_quality_low():
        """the testcase for the low air quality."""
        with patch("pedalbremsemotor.python.src.main.alerting.alert.send_alert") \
                as patched_function:
            air_quality = "200"
            alert.check_co2(air_quality)
        patched_function.assert_called_with('air-quality', level.LOW, float(air_quality))

    @staticmethod
    def test_air_quality_medium():
        """the testcase for the medium air quality."""
        with patch("pedalbremsemotor.python.src.main.alerting.alert.send_alert") \
                as patched_function:
            air_quality = "1500"
            alert.check_co2(air_quality)
        patched_function.assert_called_with('air-quality', level.MEDIUM, float(air_quality))

    @staticmethod
    def test_air_quality_high():
        """the testcase for the critical air quality."""
        with patch("pedalbremsemotor.python.src.main.alerting.alert.send_alert") \
                as patched_function:
            air_quality = "5000"
            alert.check_co2(air_quality)
        patched_function.assert_called_with('air-quality', level.CRITICAL, float(air_quality))
