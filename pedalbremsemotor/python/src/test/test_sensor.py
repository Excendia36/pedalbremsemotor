"""This module handles the unit test for the Sensor."""

import unittest
from unittest import mock
from pedalbremsemotor.python.src.main.sensor import sensor


class SensorTestCase(unittest.TestCase):
    """the Class for the Sensor Test Case."""

    def test_get_sensor_data(self):
        """the testcase for the gathering of sensor data."""
        ser_mock = mock.Mock()
        ser_mock.readline = mock.Mock(return_value="sensor_data".encode('utf-8'))
        self.assertEqual(sensor.get_sensor_data(ser_mock, b'test'), "sensor_data")
