"""This module handles the unit test for the Database."""

import unittest
from unittest.mock import patch, Mock
import mysql.connector
from pedalbremsemotor.python.src.main.handler import database_handler
from pedalbremsemotor.python.src.main.objects import room_metric


class DatabaseTestCase(unittest.TestCase):
    """the Class for the Database Test Case."""

    @patch('mysql.connector')
    def test_database_connection(self, mock_sql):
        """the testcase for the database connection."""
        self.assertIs(mysql.connector, mock_sql)
        connection = Mock()
        mock_sql.connect.return_value = connection

        metrics = room_metric.RoomMetric('23', '50', '430')
        database_handler.insert_room_metrics(metrics)

        mock_sql.connect.assert_called_with(host='localhost', database='Pedalbremsemotor',
                                            user='python_script', password="8.eD-?8ALR%t2{uy")
